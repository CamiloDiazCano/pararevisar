from django.urls import path, include
from .views import views
from .views.productsView import productsDetailView, productsCreateView, productsDeleteView, productsUpdateView
from .views.proveedorView import proveedorCreateView, proveedorDetailView, proveedorUpdateView, proveedorDeleteView
from .views.userCreateView import userCreateView
from .views.userDetailView import UserDetailView
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

urlpatterns = [
    path('user/', userCreateView.as_view()),
    path('login/', TokenObtainPairView.as_view()),
    path('user/<int:pk>/', UserDetailView.as_view()),
    path('proveedor/create/', proveedorCreateView.as_view(), name='proveedorCreate'),
    path('proveedor/update/', proveedorUpdateView.as_view(), name='proveedorUpdate'),
    path('proveedor/delete/', proveedorDeleteView.as_view(), name='proveedorDelete'),
    path('proveedor/', proveedorDetailView.as_view(), name='proveedor'),
    path('productos/', productsDetailView.as_view(), name='productos'),
    path('productos/create/', productsCreateView.as_view(), name='productosCreate'),
    path('productos/update/', productsUpdateView.as_view(), name='productosUpdate'),
    path('productos/delete/', productsDeleteView.as_view(), name='productosDelete'),
    # path('productos/mujer', views.productosMujer, name='productosMujer'),
    # path('productos/hombre', views.productosHombre, name='productosHombre'),
    # path('productos/niño', views.productosNiño, name='productosNiño'),
    # path('productos/niña', views.productosNiña, name='productosNiña'),
    # path('productos/bebe', views.productosBebe, name='productosBebe'),
    # path('productos/ofertas', views.productosOfertas, name='productosOfertas'),
    # path('productos/ultimos', views.productosUltimos, name='productosUltimos'),
    path('acercaDe', views.acerdaDe, name='acerdaDe'),
    path('', views.inicio, name='inicio'),
]
