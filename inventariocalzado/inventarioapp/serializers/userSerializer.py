from rest_framework import serializers
from inventarioapp.models.user import user

class userSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = user
        fields = ['id', 'username', 'password', 'name', 'email',]

    def create(self, validated_data):
        userInstance = user.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = user.objects.get(id=obj.id)     
        return {
                    'id': user.id, 
                    'username': user.username,
                    'name': user.name,
                    'email': user.email,
                    # 'account': {
                    #     'id': account.id,
                    #     'balance': account.balance,
                    #     'lastChangeDate': account.lastChangeDate,
                    #     'isActive': account.isActive
                    # }
                }
