from inventarioapp.models.proveedores import proveerdores
from rest_framework import serializers

class proveedorSerializer(serializers.ModelSerializer):
    class Meta:
        model = proveerdores
        fields = ['nombre_proveedor','celular_proveedor','img_url_proveedor']

    def to_representation(self, obj):
        proveerdor = proveerdores.objects.get(id_proveedor=obj.id_proveedor)

        return{
            'id_proveedor':proveerdor.id_proveedor,
            'nombre_proveedor' : proveerdor.nombre_proveedor,
            'celular_proveedor' : proveerdor.celular_proveedor,
            'img_url_proveedor': proveerdor.img_url_proveedor,
        }