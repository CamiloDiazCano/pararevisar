from inventarioapp.models.products import productos
from inventarioapp.models.proveedores import proveerdores
from rest_framework import serializers

class productsSerializer(serializers.ModelSerializer):
    class Meta:
        model = productos
        fields = ['id_producto', 'referencia_producto','nombre_producto', 'costo_producto', 'precio_venta_producto', 'oferta_producto', 'publicado', 
        'stock_producto','img_url_producto','proveedor_producto']

        def to_representation(self, obj):
             products = productos.objects.get(id=obj).productos

             return {
                 'idProducto': products.id_producto,
                 'referenciaProducto': products.referencia_producto,
                 'nombreProducto': products.nombre_producto,
                 'costoProducto': products.costo_producto,
                 'precioVenta' : products.precio_venta_producto,
                 'ofertaProducto': products.oferta_producto,
                 'publicado' : products.publicado,
                 'stock' : products.stock_producto,
                 'ImgProducto' : products.img_url_producto,
                 'proveedor': products.proveedor_producto,
                 
             }

