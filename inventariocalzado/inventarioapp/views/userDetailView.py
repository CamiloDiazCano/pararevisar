from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.exceptions import ValidationError
from django.http import response #importar diferentes paquetes de datos.
from rest_framework import generics, serializers, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from inventarioapp.models.user import user
from inventarioapp.serializers.userSerializer import userSerializer

class UserDetailView(generics.RetrieveAPIView):
    queryset            = user.objects.all()
    serializer_class    = userSerializer
    permissions_classes = [IsAuthenticated]


    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        if valid_data['user_id']!=kwargs['pk']:
            stringResponse =  {'detail': 'Unathorized Request'}
            return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)

        return super().get(request,*args, **kwargs)


