from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from inventarioapp.serializers.userSerializer import userSerializer

class userCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        seriallizer = userSerializer(data = request.data)
        seriallizer.is_valid(raise_exception=True)
        seriallizer.save()

        tokenData ={"username":request.data["username"],
                    "password":request.data["password"]}

        tokenSerializer = TokenObtainPairSerializer(data = tokenData)
        tokenSerializer.is_valid(raise_exception =True)

        return Response(tokenSerializer.validated_data, status = status.HTTP_201_CREATED)
        