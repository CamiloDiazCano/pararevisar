from django.shortcuts import render
from django.http import HttpResponse

def inicio(request):
    return HttpResponse("Pagina de inicio tienda de calzado")
def productos(request):
    return HttpResponse("todos los productos")
def productosMujer(request):
    return HttpResponse("productos mujer")
def productosHombre(request):
    return HttpResponse("productos hombre")
def productosNiño(request):
    return HttpResponse("productos niño")
def productosNiña(request):
    return HttpResponse("productos niña")
def productosBebe(request):
    return HttpResponse("productos bebe")
def productosOfertas(request):
    return HttpResponse("productos en oferta")
def productosUltimos(request):
    return HttpResponse("Ultimos Productos")
def acerdaDe(request):
    return HttpResponse("Informacion sobre la marca")