from ctypes import get_errno
from django.conf    import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from inventarioapp.models.proveedores import proveerdores
from inventarioapp.serializers import proveedorSerializer

class proveedorDetailView(generics.ListAPIView):
    queryset =  proveerdores.objects.all()
    serializer_class = proveedorSerializer
    permission_class = (IsAuthenticated)

    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)

        return super().get(request,*args, **kwargs)

class proveedorCreateView(generics.CreateAPIView):
    serializer_class = proveedorSerializer
    permission_class = (IsAuthenticated)

    def post(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)
    
        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)
    
        serializer = proveedorSerializer(data=request.data)
        serializer.is_valid(raise_exception = True)
        serializer.save()

        return Response("Transacción exitosa", status = status.HTTP_201_CREATED)

class proveedorUpdateView(generics.UpdateAPIView): #ACTUALIZAR
    serializer_class = proveedorSerializer
    permission_class = (IsAuthenticated)
    queryset =  proveerdores.objects.all()

    def update(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)
        pk = request.data.get('id')
        if (kwargs.get('pk') is not None):
            kwargs['pk'] = request.data.get('id')
        self.kwargs['pk'] = request.data.get('id')
        return super().update(request, *args, *kwargs)

class proveedorDeleteView(generics.DestroyAPIView): #ELIMINAR
    serializer_class = proveedorSerializer
    permission_class = (IsAuthenticated)
    queryset =  proveerdores.objects.all()

    def delete(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)
        pk = request.data.get('id')
        if (kwargs.get('pk') is not None):
            kwargs['pk'] = request.data.get('id')
        self.kwargs['pk'] = request.data.get('id')
        return super().delete(request, *args, *kwargs)


        