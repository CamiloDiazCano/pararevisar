from ctypes import get_errno
from django.conf    import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from inventarioapp.models.products import productos
from inventarioapp.serializers.productsSerializer import productsSerializer

class productsDetailView(generics.ListAPIView):
    queryset =  productos.objects.all()
    serializer_class = productsSerializer
    #permission_class = (IsAuthenticated)

    def get(self, request, *args, **kwargs):
        # token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        # valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)

        return super().get(request,*args, **kwargs)

class productsCreateView(generics.CreateAPIView):
    serializer_class = productsSerializer
    permission_class = (IsAuthenticated)

    def post(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)

        serializer = productsSerializer(data=request.data)
        serializer.is_valid(raise_exception = True)
        serializer.save()

        return Response("Transacción exitosa", status = status.HTTP_201_CREATED)

class productsUpdateView(generics.UpdateAPIView): #ACTUALIZAR
    serializer_class = productsSerializer
    permission_class = (IsAuthenticated)
    queryset =  productos.objects.all()

    def update(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)
        pk = request.data.get('id_producto')
        if (kwargs.get('pk') is not None):
            kwargs['pk'] = request.data.get('id_producto')
        self.kwargs['pk'] = request.data.get('id_producto')
        return super().update(request, *args, *kwargs)

class productsDeleteView(generics.DestroyAPIView): #ELIMINAR
    serializer_class = productsSerializer
    permission_class = (IsAuthenticated)
    queryset =  productos.objects.all()

    def delete(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
        valid_data = TokenBackend(algorithm='HS256').decode(token,verify=False)

        # if valid_data['user_id']!=kwargs['pk']:
        #     stringResponse =  {'detail': 'Unathorized Request'}
        #     return Response(stringResponse, status= status.HTTP_401_UNAUTHORIZED)
        pk = request.data.get('id_producto')
        if (kwargs.get('pk') is not None):
            kwargs['pk'] = request.data.get('id_producto')
        self.kwargs['pk'] = request.data.get('id_producto')
        return super().delete(request, *args, **kwargs)
