from django.contrib import admin
from .models.user import user
from .models.products import productos
from .models.proveedores import proveerdores

admin.site.register(user)
admin.site.register(productos)
admin.site.register(proveerdores)

# Register your models here.
