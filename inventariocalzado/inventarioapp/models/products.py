from django.db import models
from django.db.models.base import Model
from inventarioapp.models.proveedores import proveerdores
from .user import user
#from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
#from django.contrib.auth.hashers import make_password

class productos(models.Model):
    id_producto = models.AutoField(primary_key=True)
    referencia_producto = models.IntegerField('refProd')
    nombre_producto = models.CharField('NomProducto', max_length=80)
    costo_producto = models.IntegerField('CostProd')
    precio_venta_producto = models.IntegerField('precioVenta')
    oferta_producto = models.IntegerField('Oferta')
    publicado = models.BooleanField(default=True)
    stock_producto = models.IntegerField('Stock')
    img_url_producto = models.CharField('ImgProducto', max_length=250)
    proveedor_producto = models.ForeignKey(proveerdores, related_name='NomProveedor', on_delete=models.CASCADE)
    
    def get_object(self):
        return productos.objects.get(id_producto=request.data.get('id_producto'))