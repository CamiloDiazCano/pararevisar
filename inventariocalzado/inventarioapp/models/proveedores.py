from django.db import models
from django.db.models.base import Model
from .user import user

class proveerdores(models.Model):
    id_proveedor = models.AutoField(primary_key=True)
    nombre_proveedor = models.CharField('NomProveedor', max_length=80)
    celular_proveedor = models.CharField('CellProveedor', max_length=10)
    img_url_proveedor = models.CharField('ImgProveedor', max_length=250)