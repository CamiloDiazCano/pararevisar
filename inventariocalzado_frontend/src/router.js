import {createRouter,createWebHistory}   from 'vue-router'
import  App from    './App.vue'

import  home from    './components/home.vue'
import product from './components/product.vue'

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import admin from './components/Admin.vue'
import agregar from './components/agregar.vue'
import editar from './components/edit.vue'


const routes = [
    {
        path:       '/root',
        name:       'root',
        component:  App,
    },
    {
        path:       '/',
        name:       'home',
        component:  home,
    },
    {
        path:       '/product',
        name:       'product',
        component:  product,
    },

    {
        path: '/logIn',
        name: "logIn",
        component: ()=> import(/**/'./components/LogIn.vue' )  
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp
    },
    {
        path: '/admin',
        name: "admin",
        component: admin
    },
    {
        path: '/agregar',
        name: "agregar",
        component: agregar
    },
    {
        path: '/editar',
        name: "editar",
        component: editar
    },
    

]

const router = createRouter({
    history:    createWebHistory(),
    routes
});
  
export default router;
